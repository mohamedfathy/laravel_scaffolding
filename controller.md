# Controllers

## Defining Controller
```
php artisan make:controller ProductController
```

## Using Models inside controllers
```
use Illuminate\Http\Request;
use App\Http\Controllers\HelperController;
use App\User;
use Illuminate\Validation\Rule;
```

## Index Methods
- inside controllers/Index_methods.md

### Create Methods
- inside controllers/Create_methods.md

### Show and Delete Methods
- inside controllers/Show_methods.md

### Update Methods
- inside controllers/Update_methods.md