# Index Methods
## Bootstrap Tables with pagination
- inside ProductController.php
```
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function index()
{
    $products = Product::paginate(15);
    return view('dashboard.products.index', compact('products'));
}
```
- inside index.blade.php
```
<div class="col-12">
    <div class="pull-left"><h4>Index Products</h4></div>
    <div class="pull-right"><a href="/products/create" class="btn btn-primary btn-xs">Add Product</a></div>
    <div class="clearfix"></div>
    @if(Session::has('status'))
    <script> swal("Good job!", "{{Session::get('status')}}", "success");</script>
    @endif


    <hr />
    <table class="table table-striped">
        <thead>
            <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Quality</th>
            <th scope="col">Price</th>
            <th scope="col">Quantity</th>
            <th>Operations</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($products as $product)
            <tr>
            <th>{{$product->id}}</th>
            <td>{{$product->name}}</td>
            <td>{{$product->quality}}</td>
            <td>{{$product->suggested_price}}</td>
            <td>{{$product->quantity}}</td>
            <td>
                <a href="/products/{{$product->id}}/edit" class="btn btn-primary btn-xs">Update</a>
                <a href="/products/{{$product->id}}" class="btn btn-primary btn-xs">Show</a>
            </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{ $products->links() }}
</div>
```
## Using DataTables
- inside ProductController.php
```
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function index()
{
    $products = Product::all();
    return view('dashboard.products.index', compact('products'));
}
```
- inside index.blade.php
```
<div class="col-12">
    <div class="pull-left"><h4>Index Products</h4></div>
    <div class="pull-right"><a href="/users/create" class="btn btn-primary btn-xs">Add</a></div>
    <div class="clearfix"></div>
    @if(Session::has('status'))
    <script> swal("Good job!", "{{Session::get('status')}}", "success");</script>
    @endif

    <hr />
    <table id="data-table" class="table table-striped">
        <thead>
            <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Phone</th>
            <th scope="col">Gender</th>
            <th>Operations</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($users as $user)
            <tr>
            <th>{{$user->id}}</th>
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>
            <td>{{$user->phone}}</td>
            <td>{{$user->gender}}</td>
            <td>
                <a href="/users/{{$user->id}}/edit" class="btn btn-primary btn-xs">Update</a>
                <a href="/users/{{$user->id}}" class="btn btn-primary btn-xs">Show</a>
            </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
```