# Create Methods
## Normal Creation
- inside ProductController.php
```
/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/
public function create()
{
    //$Role = Role::all();
    //return view('dashboard.products.create', compact('Role'));
    return view('dashboard.products.create');
}
```

- inside create.blade.php
```
<div class="col-12">
  <h4>Create User</h4>
  <hr />

  <div style="padding:25px;background-color:#fff;border-radius:5px;">
  <form action="/users" method="post" enctype="multipart/form-data">
    @csrf

    <!-- // text input -->
    <div class="form-group col-md-12 mb-3">
        <label for="sample" class="sr-only">{{ ucwords('sample') }}</label>
        <input type="text" class="form-control @if ($errors->has('sample')) has-error @endif"
        id="sample" name="sample" placeholder="{{ ucwords('sample') }}" value="{{ old('sample') }}"  required>
        @if ($errors->has('sample')) 
        <span class="text-danger mx-2"> {{ $errors->first('sample') }} </span>
        @endif
    </div>

    <!-- // email input -->
    <div class="form-group col-md-12 mb-3">
        <label for="sample" class="sr-only">{{ ucwords('sample') }}</label>
        <input type="email" class="form-control @if ($errors->has('sample')) has-error @endif"
        id="sample" name="sample" placeholder="{{ ucwords('sample') }}" value="{{ old('sample') }}"  required>
        @if ($errors->has('sample')) 
        <span class="text-danger mx-2"> {{ $errors->first('sample') }} </span>
        @endif
    </div>

    <!-- // tel input -->
    <div class="form-group col-md-12 mb-3">
        <label for="sample" class="sr-only">{{ ucwords('sample') }}</label>
        <input type="tel" class="form-control @if ($errors->has('sample')) has-error @endif"
        id="sample" name="sample" placeholder="{{ ucwords('sample') }}" value="{{ old('sample') }}"  required>
        @if ($errors->has('sample')) 
        <span class="text-danger mx-2"> {{ $errors->first('sample') }} </span>
        @endif
    </div>

    <!-- // password input -->
    <div class="form-group col-md-12 mb-3">
        <label for="password" class="sr-only">{{ ucwords('password') }}</label>
        <input type="password" class="form-control @if ($errors->has('password')) has-error @endif"
        id="password" name="password" placeholder="{{ ucwords('password') }}" value="{{ old('password') }}" required>
        @if ($errors->has('password')) 
        <span class="text-danger mx-2"> {{ $errors->first('password') }} </span>
        @endif
    </div>

    <!-- // password input -->
    <div class="form-group col-md-12 mb-3">
        <label for="password_confirmation" class="sr-only">{{ ucwords('password confirmation') }}</label>
        <input type="password" class="form-control @if ($errors->has('password_confirmation')) has-error @endif"
        id="password_confirmation" name="password_confirmation" placeholder="{{ ucwords('password confirmation') }}" value="{{ old('password_confirmation') }}" required>
        @if ($errors->has('password_confirmation')) 
        <span class="text-danger mx-2"> {{ $errors->first('password_confirmation') }} </span>
        @endif
    </div>

    <!-- // number input -->
    <div class="form-group col-md-12 mb-3">
        <label for="sample" class="sr-only">{{ ucwords('sample') }}</label>
        <input type="number" class="form-control @if ($errors->has('sample')) has-error @endif"
        id="sample" name="sample" placeholder="{{ ucwords('sample') }}" value="{{ old('sample') }}"  required>
        @if ($errors->has('sample')) 
        <span class="text-danger mx-2"> {{ $errors->first('sample') }} </span>
        @endif
    </div>

    <!-- // file input -->
    <div class="form-group col-md-12 mb-3">
        <label for="sample" class="sr-only">{{ ucwords('sample') }}</label>
        <input type="file" class="form-control-file @if ($errors->has('sample')) has-error @endif"
        id="sample" name="sample" placeholder="{{ ucwords('sample') }}" value="{{ old('sample') }}"  required>
        @if ($errors->has('sample')) 
        <span class="text-danger mx-2"> {{ $errors->first('sample') }} </span>
        @endif
    </div>

    <!-- // date input -->
    <div class="form-group col-md-12 mb-3">
        <label for="sample" class="sr-only">{{ ucwords('sample') }}</label>
        <input type="date" class="form-control @if ($errors->has('sample')) has-error @endif"
        id="sample" name="sample" placeholder="{{ ucwords('sample') }}" value="{{ old('sample') }}"  required>
        @if ($errors->has('sample')) 
        <span class="text-danger mx-2"> {{ $errors->first('sample') }} </span>
        @endif
    </div>

    <!-- // datetime local input -->
    <div class="form-group col-md-12 mb-3">
        <label for="sample" class="sr-only">{{ ucwords('sample') }}</label>
        <input type="datetime-local" class="form-control @if ($errors->has('sample')) has-error @endif"
        id="sample" name="sample" placeholder="{{ ucwords('sample') }}" value="{{ old('sample') }}"  required>
        @if ($errors->has('sample')) 
        <span class="text-danger mx-2"> {{ $errors->first('sample') }} </span>
        @endif
    </div>

    <!-- // textarea input -->
    <div class="form-group col-md-12 mb-3">
        <label for="sample" class="sr-only">{{ ucwords('sample') }}</label>
        <textarea class="form-control @if ($errors->has('sample')) has-error @endif" 
        id="sample" name="sample" rows="3" placeholder="{{ ucwords('sample') }}" required>{{ old('sample') }}</textarea>
        @if ($errors->has('sample')) 
        <span class="text-danger mx-2"> {{ $errors->first('sample') }} </span>
        @endif
    </div>

    <!-- // static select  -->
    <div class="form-group col-md-12 mb-3">
        <label for="sample" class="sr-only">{{ ucwords('sample') }}</label>
        <select class="form-control custom-select @if ($errors->has('sample')) has-error @endif" 
        id="sample" name="sample" required>
        <option disabled selected value> -- select an option -- </option>
        <option value="egyptian" @if(old('sample') == "egyptian") selected @endif>{{ ucwords('egyptian') }}</option>
        <option value="hindi" @if(old('sample') == "hindi") selected @endif>{ ucwords('egyptian') }}</option>
        <option value="american" @if(old('sample') == "american") selected @endif>{ ucwords('american') }}</option>
        </select>
        @if ($errors->has('sample')) 
        <span class="text-danger mx-2"> {{ $errors->first('sample') }} </span>
        @endif
    </div>

    <!-- // dynamic select    -->
    <div class="form-group col-md-12 mb-3">
        <label for="sample" class="sr-only">{{ ucwords('sample') }}</label>
        <select class="form-control custom-select @if ($errors->has('sample')) has-error @endif" 
        id="sample" name="sample" required>
        <option disabled selected value> -- select an option -- </option>
        {{-- @foreach ($items as $item)
        <option value="{{$item->id}}"  @if ($Agent->nationality_id === $item->id) selected @endif >{{$item->name}}</option>
        @endforeach --}}
        </select>
        @if ($errors->has('sample')) 
        <span class="text-danger mx-2"> {{ $errors->first('sample') }} </span>
        @endif
    </div>

    <!-- // checkbox stacked -->
    <div class="form-group col-md-12 mb-3">
        <label for="sample" class="sr-only">{{ ucwords('sample') }}</label><br />
        <div class="custom-control custom-checkbox @if ($errors->has('chk1')) has-error @endif">
        <input type="checkbox" class="custom-control-input" value="chk1" id="chk1" name="chk1" {{ old('chk1') == 'chk1' ? 'checked' : '' }}>
        <label class="custom-control-label" for="chk1">{{ ucwords('chk1') }}</label>
        @if ($errors->has('chk1')) 
        <span class="text-danger mx-2"> {{ $errors->first('chk1') }} </span>
        @endif
        </div>
        <div class="custom-control custom-checkbox @if ($errors->has('chk2')) has-error @endif">
        <input type="checkbox" class="custom-control-input" value="chk2" id="chk2" name="chk2" {{ old('chk2') == 'chk2' ? 'checked' : '' }}>
        <label class="custom-control-label" for="chk2">{{ ucwords('chk2') }}</label>
        @if ($errors->has('chk2')) 
        <span class="text-danger mx-2"> {{ $errors->first('chk2') }} </span>
        @endif
        </div>
    </div>

    <!-- // checkbox inline -->
    <div class="form-group col-md-12 mb-3">
        <label for="sample" class="sr-only">{{ ucwords('sample') }}</label><br />
        <div class="custom-control custom-checkbox custom-control-inline" >
        <input type="checkbox" class="custom-control-input" value="chk3" id="chk3" name="chk3" {{ old('chk3') == 'chk3' ? 'checked' : '' }}>
        <label class="custom-control-label" for="chk3">{{ ucwords('chk3') }}</label>
        @if ($errors->has('chk3')) 
        <span class="text-danger mx-2"> {{ $errors->first('chk3') }} </span>
        @endif
        </div>
        <div class="custom-control custom-checkbox custom-control-inline" >
        <input type="checkbox" class="custom-control-input" value="chk4" id="chk4" name="chk4" {{ old('chk4') == 'chk4' ? 'checked' : '' }}>
        <label class="custom-control-label" for="chk4">{{ ucwords('chk4') }}</label>
        @if ($errors->has('chk4')) 
        <span class="text-danger mx-2"> {{ $errors->first('chk4') }} </span>
        @endif
        </div>
    </div>

    <!-- // radio stacked -->
    <div class="form-group col-md-12 mb-3">
        <label for="sample" class="sr-only">{{ ucwords('sample') }}</label><br />
        <div class="custom-control custom-radio">
        <input type="radio" class="custom-control-input" value="value1" id="value1" name="sample" {{ old('sample') == 'value1' ? 'checked' : '' }}>
        <label class="custom-control-label" for="value1">{{ ucwords('value1') }}</label>
        </div>
        <div class="custom-control custom-radio">
        <input type="radio" class="custom-control-input" value="value2" id="value2" name="sample" {{ old('sample') == 'value2' ? 'checked' : '' }}>
        <label class="custom-control-label" for="value2">{{ ucwords('value2') }}</label>
        </div>
        @if ($errors->has('sample')) 
        <span class="text-danger mx-2"> {{ $errors->first('sample') }} </span>
        @endif
    </div>

    <!-- // radio inline -->
    <div class="form-group col-md-12 mb-3">
        <label for="sample" class="sr-only">{{ ucwords('sample') }}</label><br />
        <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" class="custom-control-input" value="value3" id="value3" name="sample" {{ old('sample') == 'value3' ? 'checked' : '' }}>
        <label class="custom-control-label" for="value3">{{ ucwords('value3') }}</label>
        </div>
        <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" class="custom-control-input" value="value4" id="value4" name="sample" {{ old('sample') == 'value4' ? 'checked' : '' }}>
        <label class="custom-control-label" for="value4">{{ ucwords('value4') }}</label>
        </div>
        @if ($errors->has('sample')) 
        <span class="text-danger mx-2"> {{ $errors->first('sample') }} </span>
        @endif
    </div>
    <hr />
    <button type="submit" class="btn btn-primary">{{ ucwords('create') }}</button>
  </form>
  </div><!--panel-body-->

  <!-- CSS Part -->
  <style>
  .has-error {border:1px solid red;}
  </style>

</div>
```


- inside ProductController.php
```
/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function store(Request $request)
{
    $request->validate([
        //text input 
        'name' => "required|min:3|max:100",
        // email input
        'email' => "required|unique:users,email",
        // phone input
        "phone" => 'required|regex:/^\+?\d[0-9-]{7,20}$/|unique:users,phone',
        // password input 
        "password" => 'required|string|min:6|confirmed',
        // image input (file)
        'image' => 'required|image',
        // date input
        "birth_date" => 'required|date|date_format:Y-m-d',
        // textarea input
        'about' => "required|min:3",
        // radio input 
        "is_active" => 'required|boolean',
        // radio input 
        "gender" => 'required|in:male,female',
        // checkbox input 
        "car" => 'nullable',
        "motorcycle" => 'nullable',
        "bicycle" => 'nullable',
        // select list input 
        "nationality" => 'required|in:Egyptian,Hindi,American',
        // numeric input
        "balance" => 'required|numeric',
    ]);
    $sets = '';
    if(isset($request->car)){$sets .= "car,";}
    if(isset($request->motorcycle)){$sets .= "motorcycle,";}
    if(isset($request->bicycle)){$sets .= "bicycle";}

    $User = new User();
    $User->name           = $request->name;
    $User->email          = $request->email;
    $User->phone          = $request->phone;
    $User->password       = bcrypt($request->password);
    $User->image          = HelperController::upload_file('image', '/uploads/users/');
    $User->birth_date     = $request->birth_date;
    $User->about          = $request->about;
    $User->is_active      = $request->is_active;
    $User->gender         = $request->gender;
    $User->vehicles       = $sets;
    $User->nationality    = $request->nationality;
    $User->balance        = $request->balance;
    $User->save();

    $request->session()->flash('status', 'User Created successfully!');
    return redirect('/users');
}
```

## ajax methods 
- inside ProductController.php
```
/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/
public function create()
{
    //$Role = Role::all();
    //return view('dashboard.products.create', compact('Role'));
    return view('dashboard.products.create');
}
```

- inside create.blade.php
```
<div class="col-12">
  <h4>Create User</h4>
  <hr />

  <div style="padding:25px;background-color:#fff;border-radius:5px;">
  <form action="/users" method="post" enctype="multipart/form-data">
    @csrf

    <!-- // text input -->
    <div class="form-group col-md-12 mb-3">
        <label for="sample" class="sr-only">{{ ucwords('sample') }}</label>
        <input type="text" class="form-control"
        id="sample" name="sample" placeholder="{{ ucwords('sample') }}" value="{{ old('sample') }}"  required>
        <div id="sample_error" class="errors" style="color:red;"></div>
    </div>

    <!-- // email input -->
    <div class="form-group col-md-12 mb-3">
        <label for="sample" class="sr-only">{{ ucwords('sample') }}</label>
        <input type="email" class="form-control"
        id="sample" name="sample" placeholder="{{ ucwords('sample') }}" value="{{ old('sample') }}"  required>
        <div id="sample_error" class="errors" style="color:red;"></div>
    </div>

    <!-- // tel input -->
    <div class="form-group col-md-12 mb-3">
        <label for="sample" class="sr-only">{{ ucwords('sample') }}</label>
        <input type="tel" class="form-control"
        id="sample" name="sample" placeholder="{{ ucwords('sample') }}" value="{{ old('sample') }}"  required>
        <div id="sample_error" class="errors" style="color:red;"></div>
    </div>

    <!-- // password input -->
    <div class="form-group col-md-12 mb-3">
        <label for="password" class="sr-only">{{ ucwords('password') }}</label>
        <input type="password" class="form-control"
        id="password" name="password" placeholder="{{ ucwords('password') }}" value="{{ old('password') }}" required>
        <div id="sample_error" class="errors" style="color:red;"></div>
    </div>

    <!-- // password input -->
    <div class="form-group col-md-12 mb-3">
        <label for="password_confirmation" class="sr-only">{{ ucwords('password confirmation') }}</label>
        <input type="password" class="form-control"
        id="password_confirmation" name="password_confirmation" placeholder="{{ ucwords('password confirmation') }}" value="{{ old('password_confirmation') }}" required>
        <div id="sample_error" class="errors" style="color:red;"></div>
    </div>

    <!-- // number input -->
    <div class="form-group col-md-12 mb-3">
        <label for="sample" class="sr-only">{{ ucwords('sample') }}</label>
        <input type="number" class="form-control"
        id="sample" name="sample" placeholder="{{ ucwords('sample') }}" value="{{ old('sample') }}"  required>
        <div id="sample_error" class="errors" style="color:red;"></div>
    </div>

    <!-- // file input -->
    <div class="form-group col-md-12 mb-3">
        <label for="sample" class="sr-only">{{ ucwords('sample') }}</label>
        <input type="file" class="form-control-file "
        id="sample" name="sample" placeholder="{{ ucwords('sample') }}" value="{{ old('sample') }}"  required>
        <div id="sample_error" class="errors" style="color:red;"></div>
    </div>

    <!-- // date input -->
    <div class="form-group col-md-12 mb-3">
        <label for="sample" class="sr-only">{{ ucwords('sample') }}</label>
        <input type="date" class="form-control"
        id="sample" name="sample" placeholder="{{ ucwords('sample') }}" value="{{ old('sample') }}"  required>
        <div id="sample_error" class="errors" style="color:red;"></div>
    </div>

    <!-- // datetime local input -->
    <div class="form-group col-md-12 mb-3">
        <label for="sample" class="sr-only">{{ ucwords('sample') }}</label>
        <input type="datetime-local" class="form-control"
        id="sample" name="sample" placeholder="{{ ucwords('sample') }}" value="{{ old('sample') }}"  required>
        <div id="sample_error" class="errors" style="color:red;"></div>
    </div>

    <!-- // textarea input -->
    <div class="form-group col-md-12 mb-3">
        <label for="sample" class="sr-only">{{ ucwords('sample') }}</label>
        <textarea class="form-control" 
        id="sample" name="sample" rows="3" placeholder="{{ ucwords('sample') }}" required>{{ old('sample') }}</textarea>
        <div id="sample_error" class="errors" style="color:red;"></div>
    </div>

    <!-- // static select  -->
    <div class="form-group col-md-12 mb-3">
        <label for="sample" class="sr-only">{{ ucwords('sample') }}</label>
        <select class="form-control custom-select " 
        id="sample" name="sample" required>
        <option disabled selected value> -- select an option -- </option>
        <option value="egyptian" @if(old('sample') == "egyptian") selected @endif>{{ ucwords('egyptian') }}</option>
        <option value="hindi" @if(old('sample') == "hindi") selected @endif>{ ucwords('egyptian') }}</option>
        <option value="american" @if(old('sample') == "american") selected @endif>{ ucwords('american') }}</option>
        </select>
        <div id="sample_error" class="errors" style="color:red;"></div>
    </div>

    <!-- // dynamic select    -->
    <div class="form-group col-md-12 mb-3">
        <label for="sample" class="sr-only">{{ ucwords('sample') }}</label>
        <select class="form-control custom-select" 
        id="sample" name="sample" required>
        <option disabled selected value> -- select an option -- </option>
        {{-- @foreach ($items as $item)
        <option value="{{$item->id}}"  @if ($Agent->nationality_id === $item->id) selected @endif >{{$item->name}}</option>
        @endforeach --}}
        </select>
        <div id="sample_error" class="errors" style="color:red;"></div>
    </div>

    <!-- // checkbox stacked -->
    <div class="form-group col-md-12 mb-3">
        <label for="sample" class="sr-only">{{ ucwords('sample') }}</label><br />
        <div class="custom-control custom-checkbox ">
        <input type="checkbox" class="custom-control-input" value="chk1" id="chk1" name="chk1" {{ old('chk1') == 'chk1' ? 'checked' : '' }}>
        <label class="custom-control-label" for="chk1">{{ ucwords('chk1') }}</label>
        <div id="sample_error" class="errors" style="color:red;"></div>
        </div>
        <div class="custom-control custom-checkbox ">
        <input type="checkbox" class="custom-control-input" value="chk2" id="chk2" name="chk2" {{ old('chk2') == 'chk2' ? 'checked' : '' }}>
        <label class="custom-control-label" for="chk2">{{ ucwords('chk2') }}</label>
        <div id="sample_error" class="errors" style="color:red;"></div>
        </div>
    </div>

    <!-- // checkbox inline -->
    <div class="form-group col-md-12 mb-3">
        <label for="sample" class="sr-only">{{ ucwords('sample') }}</label><br />
        <div class="custom-control custom-checkbox custom-control-inline" >
        <input type="checkbox" class="custom-control-input" value="chk3" id="chk3" name="chk3" {{ old('chk3') == 'chk3' ? 'checked' : '' }}>
        <label class="custom-control-label" for="chk3">{{ ucwords('chk3') }}</label>
        <div id="sample_error" class="errors" style="color:red;"></div>
        </div>
        <div class="custom-control custom-checkbox custom-control-inline" >
        <input type="checkbox" class="custom-control-input" value="chk4" id="chk4" name="chk4" {{ old('chk4') == 'chk4' ? 'checked' : '' }}>
        <label class="custom-control-label" for="chk4">{{ ucwords('chk4') }}</label>
        <div id="sample_error" class="errors" style="color:red;"></div>
        </div>
    </div>

    <!-- // radio stacked -->
    <div class="form-group col-md-12 mb-3">
        <label for="sample" class="sr-only">sample</label><br />
        <div class="custom-control custom-radio">
        <input type="radio" class="custom-control-input" value="value1" id="value1" name="sample" {{ old('sample') == 'value1' ? 'checked' : '' }}>
        <label class="custom-control-label" for="value1">{{ ucwords('value1') }}</label>
        </div>
        <div class="custom-control custom-radio">
        <input type="radio" class="custom-control-input" value="value2" id="value2" name="sample" {{ old('sample') == 'value2' ? 'checked' : '' }}>
        <label class="custom-control-label" for="value2">{{ ucwords('value2') }}</label>
        </div>

    </div>

    <!-- // radio inline -->
    <div class="form-group col-md-12 mb-3">
        <label for="sample" class="sr-only">sample</label><br />
        <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" class="custom-control-input" value="value3" id="value3" name="sample" {{ old('sample') == 'value3' ? 'checked' : '' }}>
        <label class="custom-control-label" for="value3">{{ ucwords('value3') }}</label>
        </div>
        <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" class="custom-control-input" value="value4" id="value4" name="sample" {{ old('sample') == 'value4' ? 'checked' : '' }}>
        <label class="custom-control-label" for="value4">{{ ucwords('value4') }}</label>
        </div>
    </div>
    <hr />
    <button type="submit" class="btn btn-primary">{{ ucwords('create') }}</button>
  </form>
  </div><!--panel-body-->
</div>
<script type="text/javascript">
  $(document).ready(function() {
    $("form").submit(function(evt){
        evt.preventDefault();

        // collecting all data
        // CSRF Token 
        var token        = $('input[name=_token]').val();
        // text input
        var name         = $("#name").val();
        // email input
        var email        = $("#email").val();
        // phone input
        var phone        = $("#phone").val();
        // password input
        var password     = $("#password").val();
        // password_confirmation input
        var password_confirmation     = $("#password_confirmation").val();
        // file input
        var image        = document.getElementById('image');
        // date input
        var birth_date   = $("#birth_date").val();
        // textarea input 
        var about        = $("#about").val();
        // radio input 
        var is_active    = $("input[name='is_active']:checked").val();
        // radio input 
        var gender       = $("input[name='gender']:checked").val();
        // checkbox input 
        var car          = $("#car").val();
        var motorcycle   = $("#motorcycle").val();
        var bicycle      = $("#bicycle").val();
        // select list input 
        var nationality  = $("#nationality").val();
        // number input 
        var balance      = $("#balance").val();


        // Form data Creating
        var formData = new FormData();
        // token input
        formData.append('_token', token);
        // name input
        formData.append('name', name);
        // email input
        formData.append('email', email);
        // phone input
        formData.append('phone', phone);
        // password input
        formData.append('password', password);
        // password_confirmation input
        formData.append('password_confirmation', password_confirmation);
        // file input
        formData.append('image', image.files[0], image.files[0].name);
        // date input
        formData.append('birth_date', birth_date);
        // textarea input 
        formData.append('about', about);
        // radio input
        formData.append('is_active', is_active);
        // radio input
        formData.append('gender', gender);
        // checkbox input
        formData.append('car', car);
        formData.append('motorcycle', motorcycle);
        formData.append('bicycle', bicycle);
        // select list input 
        formData.append('nationality', nationality);
        // number input 
        formData.append('balance', parseInt(balance));

        var baseUrl = 'http://127.0.0.1:8000/'
        
        // sending Ajax
        $.ajax({
          type: 'POST',
          url:  baseUrl + 'users',
          data: formData,
          cache: false,
          contentType: false,
          processData: false,
          success: function(data){
              //console.log(data)
              window.location.replace(baseUrl + "users");
          },
          error: function (data) {
            $( ".errors" ).empty();

            var responseJson = JSON.parse(data.responseText);
            $.each(responseJson.errors, function(key,value) {
              $('#'+ key +'_error').prepend('<span class="text-danger"> '+value+' </span>');
            });
          }
        }); 

    });

  });
</script>
```

- inside ProductController.php
```
/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function store(Request $request)
{
    $request->validate([
        //text input 
        'name' => "required|min:3|max:100",
        // email input
        'email' => "required|unique:users,email",
        // phone input
        "phone" => 'required|regex:/^\+?\d[0-9-]{7,20}$/|unique:users,phone',
        // password input 
        "password" => 'required|string|min:6|confirmed',
        // image input (file)
        'image' => 'required|image',
        // date input
        "birth_date" => 'required|date|date_format:Y-m-d',
        // textarea input
        'about' => "required|min:3",
        // radio input 
        "is_active" => 'required|boolean',
        // radio input 
        "gender" => 'required|in:male,female',
        // checkbox input 
        "car" => 'nullable',
        "motorcycle" => 'nullable',
        "bicycle" => 'nullable',
        // select list input 
        "nationality" => 'required|in:Egyptian,Hindi,American',
        // numeric input
        "balance" => 'required|numeric',
    ]);

    $sets = '';
    if(isset($request->car)){$sets .= "car,";}
    if(isset($request->motorcycle)){$sets .= "motorcycle,";}
    if(isset($request->bicycle)){$sets .= "bicycle";}

    $User = new User();
    $User->name           = $request->name;
    $User->email          = $request->email;
    $User->phone          = $request->phone;
    $User->password       = bcrypt($request->password);
    $User->image          = HelperController::upload_file('image', '/uploads/users/');
    $User->birth_date     = $request->birth_date;
    $User->about          = $request->about;
    $User->is_active      = $request->is_active;
    $User->gender         = $request->gender;
    $User->vehicles       = $sets;
    $User->nationality    = $request->nationality;
    $User->balance        = $request->balance;
    $User->save();

    $request->session()->flash('status', 'User Created successfully!');
}
```