# Show Methods
## Normal Show
- inside ProductController.php
```
/**
* Display the specified resource.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function show($id)
{
    $user = User::find($id);
    return view('dashboard.users.show', compact('user'));
}

/**
* Remove the specified resource from storage.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function destroy($id)
{
    global $request;
    $user = User::destroy($id);
    $request->session()->flash('status', 'User Deleted successfully!');
    return redirect('/users');
}
```
- inside show.blade.php
```
<div class="col-12">
  <h4>Show User</h4>
  <hr />

  <div style="padding:25px;background-color:#fff;border-radius:5px;">
    <div class="row">
      <div class="col-xs-3">
        <!-- image input -->
        <img src="{{$user->image}}" class="img-thumbnail"  alt="...">
      </div>
      <div class="col-xs-9">
        
        <table class="table table-hover">
          <tbody>
            <tr>
              <!-- text input -->
              <td>{{ucwords('name')}}</td>
              <td>{{$user->name}}</td>
            </tr>
            <tr>
              <!-- email input -->
              <td>{{ucwords('email')}}</td>
              <td>{{$user->email}}</td>
            </tr>
            <tr>
              <!-- phone input -->
              <td>{{ucwords('phone')}}</td>
              <td>{{$user->phone}}</td>
            </tr>
            <tr>
              <!-- date input -->
              <td>{{ucwords('birth date')}}</td>
              <td>{{$user->birth_date}}</td>
            </tr>
            <tr>
              <!-- textarea input -->
              <td>{{ucwords('about')}}</td>
              <td>{{$user->about}}</td>
            </tr>
            <tr>
              <!-- radio input -->
              <td>{{ucwords('is active')}}</td>
              <td>{{ucwords($user->is_active ? 'yes' : 'no')}}</td>
            </tr>
            <tr>
              <!-- radio input -->
              <td>{{ucwords('gender')}}</td>
              <td>{{ucwords($user->gender)}}</td>
            </tr>
            <tr>
              <!-- checkbox input -->
              <td>{{ucwords('vehicles')}}</td>
              <td>{{ucwords($user->vehicles)}}</td>
            </tr>
            <tr>
              <!-- select list input -->
              <td>{{ucwords('nationality')}}</td>
              <td>{{ucwords($user->nationality)}}</td>
            </tr>
            <tr>
              <!-- number input -->
              <td>{{ucwords('balance')}}</td>
              <td>{{$user->balance}}</td>
            </tr>
            <tr>
              <!-- datetime input -->
              <td>{{ucwords('created at')}}</td>
              <td>{{ucwords($user->created_at)}}</td>
            </tr>
            <tr>
              <!-- datetime input -->
              <td>{{ucwords('updated at')}}</td>
              <td>{{ucwords($user->updated_at)}}</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    
    <br />
    <hr />
    
    <form action="/users/{{$user->id}}" method="post">
        @csrf
        <input type="hidden" name="_method" value="DELETE">
        <a href="/users" class="btn btn-primary">{{ucwords('go back')}}</a>
        <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this item?');">{{ ucwords('delete') }}</button>
    </form>
  </div><!--panel-body-->
</div>
```
