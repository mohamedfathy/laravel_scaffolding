# Routes

## web Routes

- routes/web.php
```
// Simple CRUD
Route::get('/photos', 'PhotosController@index');
Route::get('/photos/create', 'PhotosController@create');
Route::post('/photos', 'PhotosController@store');
Route::get('/photos/{photo}', 'PhotosController@show');
Route::get('/photos/{photo}/edit', 'PhotosController@edit');
Route::put('/photos/{photo}', 'PhotosController@update');
Route::delete('/photos/{photo}', 'PhotosController@update');
```

## Adding resource routes
```
Route::resource('products','ProductController');
```
