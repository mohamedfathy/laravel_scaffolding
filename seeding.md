# Seeding

## Writing Seeders
```
php artisan make:seeder UsersTableSeeder
```

## Adding Data
```
public function run()
{
    $brands = [
        [
            'id' => 1,
            'name_ar' => 'اي بى تى',
            'name_en' => 'apt',
            'image_url' => 'uploads/brands/apt.jpeg',
            'is_active' => 1,
            'created_at' => '2020-02-26 15:46:40',
            'updated_at' => '2020-02-26 15:46:40',
            'deleted_at' => NULL,
        ],
        [
            'id' => 2,
            'name_ar' => 'بلاك بلس ديكر',
            'name_en' => 'black + deaker',
            'image_url' => 'uploads/brands/black_plus_decker.png',
            'is_active' => 1,
            'created_at' => '2020-02-26 15:46:40',
            'updated_at' => '2020-02-26 15:46:40',
            'deleted_at' => NULL,
        ]
    ];

    DB::table('brands')->insert($brands);
}
```

## Calling Additional Seeders
- Within the DatabaseSeeder class, you may use the call method to execute additional seed classes.
```
public function run()
{
    $this->call([
        BillingTableSeeder::class,
    ]);
}
```

## Running Seeders
```
composer dump-autoload
php artisan db:seed
```