<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 100);
            $table->string('email')->unique();
            $table->string('phone', 100);
            $table->string('password');
            $table->string('image', 100);
            $table->date('birth_date');	
            $table->text('about');
            $table->boolean('is_active');
            $table->enum('gender', ['male', 'female']);		
            $table->set('vehicles', ['car', 'motorcycle', 'bicycle']);	
            $table->enum('nationality', ['Egyptian', 'Hindi', 'American']);	
            $table->decimal('balance', 10, 2);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->softDeletes();	
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
