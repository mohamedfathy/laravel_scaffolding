<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'id' => 1,
                'name' => 'Mohamed Fathy',
                'email' => Str::random(10).'@gmail.com',
                'phone' => '01008292985',
                'password' => bcrypt(123456),
                'image' => 'uploads/brands/avatar.jpeg',
                'birth_date' => '1992-08-23',
                'about' => 'Say some words',
                'is_active' => 1,
                'gender' => 'male',
                'vehicles' => 3,
                'nationality' => 'hindi',
                'balance' => 105.99,
                'created_at' => '2020-02-26 15:46:40',
                'updated_at' => '2020-02-26 15:46:40',
                'deleted_at' => NULL,
            ],
        ];
    
        DB::table('users')->insert($users);
    }
}
