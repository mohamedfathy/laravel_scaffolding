@extends ('dashboard.layouts.02_master')
@section('title', 'Update resouce')
@section ('content')
<div class="col-12">
  <h4>Update User</h4>
  <hr />

  <div style="padding:25px;background-color:#fff;border-radius:5px;">
  <form action="/users/{{$user->id}}" method="post" enctype="multipart/form-data">
    @csrf
    <input type="hidden" name="_method" value="PUT">
    <input type="hidden" id="id" name="id" value="{{ $user->id }}">


    <!-- // text input -->
    <div class="form-group col-md-12 mb-3">
        <label for="name">{{ ucwords('name') }}</label>
        <input type="text" class="form-control"
        id="name" name="name" placeholder="{{ ucwords('name') }}" value="{{ $user->name }}"  required>
        <div id="name_error" class="errors" style="color:red;"></div>
    </div>

    <!-- // email input -->
    <div class="form-group col-md-12 mb-3">
        <label for="email">{{ ucwords('email') }}</label>
        <input type="email" class="form-control"
        id="email" name="email" placeholder="{{ ucwords('email') }}" value="{{ $user->email }}"  required>
        <div id="email_error" class="errors" style="color:red;"></div>
    </div>

    <!-- // tel input -->
    <div class="form-group col-md-12 mb-3">
        <label for="phone" >{{ ucwords('phone') }}</label>
        <input type="tel" class="form-control"
        id="phone" name="phone" placeholder="{{ ucwords('phone') }}" value="{{ $user->phone }}"  required>
        <div id="phone_error" class="errors" style="color:red;"></div>
    </div>

    <!-- // password input -->
    <div class="form-group col-md-12 mb-3">
        <label for="password" >{{ ucwords('password') }}</label>
        <input type="password" class="form-control"
        id="password" name="password" placeholder="{{ ucwords('password') }}" >
        <div id="sample_error" class="errors" style="color:red;"></div>
    </div>

    <!-- // password input -->
    <div class="form-group col-md-12 mb-3">
        <label for="password_confirmation" >{{ ucwords('password confirmation') }}</label>
        <input type="password" class="form-control"
        id="password_confirmation" name="password_confirmation" placeholder="{{ ucwords('password confirmation') }}">
        <div id="sample_error" class="errors" style="color:red;"></div>
    </div>

    <!-- // file input -->
    <div class="form-group col-md-12 mb-3">
        <label for="image" >{{ ucwords('image') }}</label><br />
        <!-- image input -->
        <img src="{{$user->image}}" class="img-thumbnail"  alt="..." style="width:200px;">
        <input type="file" class="form-control-file"
        id="image" name="image">
        <div id="image_error" class="errors" style="color:red;"></div>
    </div>

    <!-- // date input -->
    <div class="form-group col-md-12 mb-3">
        <label for="birth_date" class="sr-only">{{ ucwords('birth_date') }}</label>
        <input type="date" class="form-control"
        id="birth_date" name="birth_date" placeholder="{{ ucwords('birth_date') }}" value="{{ $user->birth_date }}"  required>
        <div id="birth_date_error" class="errors" style="color:red;"></div>
    </div>

    <!-- // textarea input -->
    <div class="form-group col-md-12 mb-3">
        <label for="about" class="sr-only">{{ ucwords('about') }}</label>
        <textarea class="form-control" 
        id="about" name="about" rows="3" placeholder="{{ ucwords('about') }}" required>{{ $user->about }}</textarea>
        <div id="about_error" class="errors" style="color:red;"></div>
    </div>

    <!-- // radio stacked -->
    <div class="form-group col-md-12 mb-3">
        <label for="is_active">{{ ucwords('is active') }}</label><br />
        <div class="custom-control custom-radio">
        <input type="radio" class="custom-control-input" value="1" id="yes" name="is_active" {{ $user->is_active  == '1' ? 'checked' : '' }}>
        <label class="custom-control-label" for="1">{{ ucwords('yes') }}</label>
        </div>
        <div class="custom-control custom-radio">
        <input type="radio" class="custom-control-input" value="0" id="no" name="is_active" {{ $user->is_active  == '0' ? 'checked' : '' }}>
        <label class="custom-control-label" for="0">{{ ucwords('no') }}</label>
        </div>
        <div id="is_active_error" class="errors" style="color:red;"></div>
    </div>

    <!-- // radio stacked -->
    <div class="form-group col-md-12 mb-3">
        <label for="gender" >{{ ucwords('gender') }}</label><br />
        <div class="custom-control custom-radio">
        <input type="radio" class="custom-control-input" value="male" id="male" name="gender" {{ $user->gender  == 'male' ? 'checked' : '' }}>
        <label class="custom-control-label" for="male">{{ ucwords('male') }}</label>
        </div>
        <div class="custom-control custom-radio">
        <input type="radio" class="custom-control-input" value="female" id="female" name="gender" {{ $user->gender  == 'female' ? 'checked' : '' }}>
        <label class="custom-control-label" for="female">{{ ucwords('female') }}</label>
        </div>
        <div id="gender_error" class="errors" style="color:red;"></div>
    </div>

    <!-- // checkbox stacked -->
    <div class="form-group col-md-12 mb-3">
        @php
            $pieces = explode(",", $user->vehicles);
        @endphp
        <label for="vehicles">{{ ucwords('vehicles') }}</label><br />
        <div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input" value="car" id="car" name="car" {{ in_array("car", $pieces) ? 'checked' : '' }}>
        <label class="custom-control-label" for="car">{{ ucwords('car') }}</label>
        <div id="car_error" class="errors" style="color:red;"></div>
        </div>
        <div class="custom-control custom-checkbox ">
        <input type="checkbox" class="custom-control-input" value="motorcycle" id="motorcycle" name="motorcycle" {{ in_array("motorcycle", $pieces) ? 'checked' : '' }}>
        <label class="custom-control-label" for="motorcycle">{{ ucwords('motorcycle') }}</label>
        <div id="motorcycle_error" class="errors" style="color:red;"></div>
        </div>
        <div class="custom-control custom-checkbox ">
        <input type="checkbox" class="custom-control-input" value="bicycle" id="bicycle" name="bicycle" {{ in_array("bicycle", $pieces) ? 'checked' : '' }}>
        <label class="custom-control-label" for="bicycle">{{ ucwords('bicycle') }}</label>
        <div id="bicycle_error" class="errors" style="color:red;"></div>
        </div>
    </div>

    <!-- // static select  -->
    <div class="form-group col-md-12 mb-3">
        <label for="nationality">{{ ucwords('nationality') }}</label>
        <select class="form-control custom-select" 
        id="nationality" name="nationality" required>
        <option disabled selected value> -- select an option -- </option>
        <option value="Egyptian" @if($user->nationality == "Egyptian") selected @endif>Egyptian</option>
        <option value="Hindi" @if($user->nationality == "Hindi") selected @endif>Hindi</option>
        <option value="American" @if($user->nationality == "American") selected @endif>American</option>
        </select>
        <div id="nationality_error" class="errors" style="color:red;"></div>
    </div>

    <!-- // number input -->
    <div class="form-group col-md-12 mb-3">
        <label for="balance">{{ ucwords('balance') }}</label>
        <input type="number" class="form-control"
        id="balance" name="balance" placeholder="{{ ucwords('balance') }}" value="{{ $user->balance }}"  required>
        <div id="balance_error" class="errors" style="color:red;"></div>
    </div>

    <hr />
    <button type="submit" class="btn btn-primary">{{ ucwords('update') }}</button>
  </form>
  </div><!--panel-body-->
</div>
<script type="text/javascript">
  $(document).ready(function() {
    $("form").submit(function(evt){
        evt.preventDefault();

        // collecting all data
        // CSRF Token 
        var token        = $('input[name=_token]').val();
        // id input
        var id           = $("#id").val();
        // text input
        var name         = $("#name").val();
        // email input
        var email        = $("#email").val();
        // phone input
        var phone        = $("#phone").val();
        // password input
        var password     = $("#password").val();
        // password_confirmation input
        var password_confirmation     = $("#password_confirmation").val();
        // file input
        var image        = document.getElementById('image');
        // date input
        var birth_date   = $("#birth_date").val();
        // textarea input 
        var about        = $("#about").val();
        // radio input 
        var is_active    = $("input[name='is_active']:checked").val();
        // radio input 
        var gender       = $("input[name='gender']:checked").val();
        // checkbox input 
        var car          = $("#car").val();
        var motorcycle   = $("#motorcycle").val();
        var bicycle      = $("#bicycle").val();
        // select list input 
        var nationality  = $("#nationality").val();
        // number input 
        var balance      = $("#balance").val();


        // Form data Creating
        var formData = new FormData();
        // The most important thing to work adding this field.
        formData.append('_method', 'PUT');
        // adding token
        formData.append('_token', token);
        // id input
        formData.append('id', id);
        // name input
        formData.append('name', name);
        // email input
        formData.append('email', email);
        // phone input
        formData.append('phone', phone);
        // password input
        formData.append('password', password);
        // password_confirmation input
        formData.append('password_confirmation', password_confirmation);
        // file input
        if (image.files.length !== 0) {
            formData.append('image', image.files[0], image.files[0].name);
        }
        // date input
        formData.append('birth_date', birth_date);
        // textarea input 
        formData.append('about', about);
        // radio input
        formData.append('is_active', is_active);
        // radio input
        formData.append('gender', gender);
        // checkbox input
        formData.append('car', car);
        formData.append('motorcycle', motorcycle);
        formData.append('bicycle', bicycle);
        // select list input 
        formData.append('nationality', nationality);
        // number input 
        formData.append('balance', parseInt(balance));

        var baseUrl = 'http://127.0.0.1:8000/';

        // sending Ajax
        $.ajax({
          type: 'POST',
          url:  baseUrl + "users/" + id ,
          data: formData,
          cache: false,
          contentType: false,
          processData: false,
          headers: {'X-CSRF-TOKEN': $('input[name=_token]').val()},
          success: function(data){
              // console.log(data)
              window.location.replace(baseUrl + "users");
          },
          error: function (data) {
            $( ".errors" ).empty();

            var responseJson = JSON.parse(data.responseText);
            $.each(responseJson.errors, function(key,value) {
              $('#'+ key +'_error').prepend('<span class="text-danger"> '+ value +' </span>');
            });
          }
        }); 
    });
  });
</script>
@endsection