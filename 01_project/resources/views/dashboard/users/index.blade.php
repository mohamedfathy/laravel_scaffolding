@extends ('dashboard.layouts.02_master')
@section('title', 'Index Resource')
@section ('content')
<div class="col-12">
    <div class="pull-left"><h4>Index Products</h4></div>
    <div class="pull-right"><a href="/users/create" class="btn btn-primary btn-xs">Add</a></div>
    <div class="clearfix"></div>
    @if(Session::has('status'))
    <script> swal("Good job!", "{{Session::get('status')}}", "success");</script>
    @endif

    <hr />
    <table id="data-table" class="table table-striped">
        <thead>
            <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Phone</th>
            <th scope="col">Gender</th>
            <th>Operations</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($users as $user)
            <tr>
            <th>{{$user->id}}</th>
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>
            <td>{{$user->phone}}</td>
            <td>{{$user->gender}}</td>
            <td>
                <a href="/users/{{$user->id}}/edit" class="btn btn-primary btn-xs">Update</a>
                <a href="/users/{{$user->id}}" class="btn btn-primary btn-xs">Show</a>
            </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection