@extends ('dashboard.layouts.02_master')
@section('title', 'Create resouce')
@section ('content')
<div class="col-12">
  <h4>Create User</h4>
  <hr />

  <div style="padding:25px;background-color:#fff;border-radius:5px;">
  <form action="/users" method="post" enctype="multipart/form-data">
    @csrf

    <!-- // text input -->
    <div class="form-group col-md-12 mb-3">
        <label for="name">{{ ucwords('name') }}</label>
        <input type="text" class="form-control "
        id="name" name="name" placeholder="{{ ucwords('name') }}" value="{{ old('name') }}"  required>
        <div id="name_error" class="errors" style="color:red;"></div>
    </div>

    <!-- // email input -->
    <div class="form-group col-md-12 mb-3">
        <label for="email" >{{ ucwords('email') }}</label>
        <input type="email" class="form-control "
        id="email" name="email" placeholder="{{ ucwords('email') }}" value="{{ old('email') }}"  required>
        <div id="email_error" class="errors" style="color:red;"></div>
    </div>

    <!-- // tel input -->
    <div class="form-group col-md-12 mb-3">
        <label for="phone">{{ ucwords('phone') }}</label>
        <input type="tel" class="form-control "
        id="phone" name="phone" placeholder="{{ ucwords('phone') }}" value="{{ old('phone') }}"  required>
        <div id="phone_error" class="errors" style="color:red;"></div>
    </div>

    <!-- // password input -->
    <div class="form-group col-md-12 mb-3">
        <label for="password" >{{ ucwords('password') }}</label>
        <input type="password" class="form-control "
        id="password" name="password" placeholder="{{ ucwords('password') }}" value="{{ old('password') }}" required>
        <div id="password_error" class="errors" style="color:red;"></div>
    </div>

    <!-- // password input -->
    <div class="form-group col-md-12 mb-3">
        <label for="password_confirmation" >{{ ucwords('password confirmation') }}</label>
        <input type="password" class="form-control"
        id="password_confirmation" name="password_confirmation" placeholder="{{ ucwords('password confirmation') }}" value="{{ old('password_confirmation') }}" required>
        <div id="password_confirmation_error" class="errors" style="color:red;"></div>
    </div>

    <!-- // file input -->
    <div class="form-group col-md-12 mb-3">
        <label for="image" >{{ ucwords('image') }}</label>
        <input type="file" class="form-control-file "
        id="image" name="image" placeholder="{{ ucwords('image') }}" value="{{ old('image') }}"  required>
        <div id="image_error" class="errors" style="color:red;"></div>
    </div>

    <!-- // datetime input -->
    <div class="form-group col-md-12 mb-3">
        <label for="birth_date" >{{ ucwords('birth date') }}</label>
        <input type="date" class="form-control "
        id="birth_date" name="birth_date" placeholder="{{ ucwords('birth date') }}" value="{{ old('birth_date') }}"  required>
        <div id="birth_date_error" class="errors" style="color:red;"></div>
    </div>

    <!-- // textarea input -->
    <div class="form-group col-md-12 mb-3">
        <label for="about" >{{ ucwords('about') }}</label>
        <textarea class="form-control " 
        id="about" name="about" rows="3" placeholder="{{ ucwords('about') }}" required>{{ old('about') }}</textarea>
        <div id="about_error" class="error" style="color:red;"></div>
    </div>

    <!-- // radio inline -->
    <div class="form-group col-md-12 mb-3">
        <label for="is_active">Active</label><br />
        <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" class="custom-control-input" value="1" id="active" name="is_active" {{ old('is_active') == 1 ? 'checked' : '' }}>
        <label class="custom-control-label" for="active">{{ ucwords('active') }}</label>
        </div>
        <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" class="custom-control-input" value="0" id="not_active" name="is_active" {{ old('is_active') == 0 ? 'checked' : '' }}>
        <label class="custom-control-label" for="not_active">{{ ucwords('not active') }}</label>
        </div>
        <div id="is_active_error" class="errors" style="color:red;"></div>
    </div>   

    <!-- // radio inline -->
    <div class="form-group col-md-12 mb-3">
        <label for="gender">Gender</label><br />
        <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" class="custom-control-input" value="male" id="male" name="gender" {{ old('gender') == 'male' ? 'checked' : '' }}>
        <label class="custom-control-label" for="male">{{ ucwords('male') }}</label>
        </div>
        <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" class="custom-control-input" value="female" id="female" name="gender" {{ old('gender') == 'female' ? 'checked' : '' }}>
        <label class="custom-control-label" for="female">{{ ucwords('female') }}</label>
        </div>
        <div id="gender_error" class="errors" style="color:red;"></div>
    </div>    

    <!-- // checkbox stacked -->
    <div class="form-group col-md-12 mb-3">
        <label for="vehicles">{{ ucwords('vehicles') }}</label><br />
        <div class="custom-control custom-checkbox ">
        <input type="checkbox" class="custom-control-input" value="1" id="car" name="car" {{ old('car') == '1' ? 'checked' : '' }}>
        <label class="custom-control-label" for="car">{{ ucwords('car') }}</label>
        <div id="car_error" class="errors" style="color:red;"></div>
        </div>
        <div class="custom-control custom-checkbox @if ($errors->has('motorcycle')) has-error @endif">
        <input type="checkbox" class="custom-control-input" value="2" id="motorcycle" name="motorcycle" {{ old('motorcycle') == '2' ? 'checked' : '' }}>
        <label class="custom-control-label" for="motorcycle">{{ ucwords('motorcycle') }}</label>
        <div id="motorcycle_error" class="errors" style="color:red;"></div>
        </div>

        <div class="custom-control custom-checkbox @if ($errors->has('bicycle')) has-error @endif">
        <input type="checkbox" class="custom-control-input" value="3" id="bicycle" name="bicycle" {{ old('bicycle') == '3' ? 'checked' : '' }}>
        <label class="custom-control-label" for="bicycle">{{ ucwords('bicycle') }}</label>
        <div id="bicycle_error" class="errors" style="color:red;"></div>
        </div>
    </div>

    <!-- // static select  -->
    <div class="form-group col-md-12 mb-3">
        <label for="nationality" >{{ ucwords('nationality') }}</label>
        <select class="form-control custom-select " 
        id="nationality" name="nationality" required>
        <option disabled selected value> -- select an option -- </option>
        <option value="Egyptian" @if(old('nationality') == "Egyptian") selected @endif>{{ ucwords('egyptian') }}</option>
        <option value="Hindi" @if(old('nationality') == "Hindi") selected @endif>{{ ucwords('hindi') }}</option>
        <option value="American" @if(old('nationality') == "American") selected @endif>{{ ucwords('american') }}</option>
        </select>
        <div id="nationality_error" class="errors" style="color:red;"></div>
    </div>

    <!-- // number input -->
    <div class="form-group col-md-12 mb-3">
        <label for="balance">{{ ucwords('balance') }}</label>
        <input type="number" class="form-control "
        id="balance" name="balance" placeholder="{{ ucwords('balance') }}" value="{{ old('balance') }}"  required>
        <div id="balance_error" class="errors" style="color:red;"></div>
    </div>

    <hr>
    <button type="submit" class="btn btn-primary">{{ ucwords('create') }}</button>
  </form>
  </div><!--panel-body-->
</div>

<script type="text/javascript">
  $(document).ready(function() {
    $("form").submit(function(evt){
        evt.preventDefault();

        // collecting all data
        // CSRF Token 
        var token        = $('input[name=_token]').val();
        // text input
        var name         = $("#name").val();
        // email input
        var email        = $("#email").val();
        // phone input
        var phone        = $("#phone").val();
        // password input
        var password     = $("#password").val();
        // password_confirmation input
        var password_confirmation     = $("#password_confirmation").val();
        // file input
        var image        = document.getElementById('image');
        // date input
        var birth_date   = $("#birth_date").val();
        // textarea input 
        var about        = $("#about").val();
        // radio input 
        var is_active    = $("input[name='is_active']:checked").val();
        // radio input 
        var gender       = $("input[name='gender']:checked").val();
        // checkbox input 
        var car          = $("#car").val();
        var motorcycle   = $("#motorcycle").val();
        var bicycle      = $("#bicycle").val();
        // select list input 
        var nationality  = $("#nationality").val();
        // number input 
        var balance      = $("#balance").val();


        // Form data Creating
        var formData = new FormData();
        // token input
        formData.append('_token', token);
        // name input
        formData.append('name', name);
        // email input
        formData.append('email', email);
        // phone input
        formData.append('phone', phone);
        // password input
        formData.append('password', password);
        // password_confirmation input
        formData.append('password_confirmation', password_confirmation);
        // file input
        formData.append('image', image.files[0], image.files[0].name);
        // date input
        formData.append('birth_date', birth_date);
        // textarea input 
        formData.append('about', about);
        // radio input
        formData.append('is_active', is_active);
        // radio input
        formData.append('gender', gender);
        // checkbox input
        formData.append('car', car);
        formData.append('motorcycle', motorcycle);
        formData.append('bicycle', bicycle);
        // select list input 
        formData.append('nationality', nationality);
        // number input 
        formData.append('balance', parseInt(balance));

        var baseUrl = 'http://127.0.0.1:8000/'
        
        // sending Ajax
        $.ajax({
          type: 'POST',
          url:  baseUrl + 'users',
          data: formData,
          cache: false,
          contentType: false,
          processData: false,
          success: function(data){
              console.log(data)
              window.location.replace(baseUrl + "users");
          },
          error: function (data) {
            $( ".errors" ).empty();

            var responseJson = JSON.parse(data.responseText);
            $.each(responseJson.errors, function(key,value) {
              $('#'+ key +'_error').prepend('<span class="text-danger"> '+value+' </span>');
            });
          }
        }); 

    });

  });
</script>
@endsection