<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\HelperController;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $users = User::all();
        return view('dashboard.users.index', compact('users'));
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        return view('dashboard.users.create');
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $request->validate([
            //text input 
            'name' => "required|min:3|max:100",
            // email input
            'email' => "required|unique:users,email",
            // phone input
            "phone" => 'required|regex:/^\+?\d[0-9-]{7,20}$/|unique:users,phone',
            // password input 
            "password" => 'required|string|min:6|confirmed',
            // image input (file)
            'image' => 'required|image',
            // date input
            "birth_date" => 'required|date|date_format:Y-m-d',
            // textarea input
            'about' => "required|min:3",
            // radio input 
            "is_active" => 'required|boolean',
            // radio input 
            "gender" => 'required|in:male,female',
            // checkbox input 
            "car" => 'nullable',
            "motorcycle" => 'nullable',
            "bicycle" => 'nullable',
            // select list input 
            "nationality" => 'required|in:Egyptian,Hindi,American',
            // numeric input
            "balance" => 'required|numeric',
        ]);

        $sets = '';
        if(isset($request->car)){$sets .= "car,";}
        if(isset($request->motorcycle)){$sets .= "motorcycle,";}
        if(isset($request->bicycle)){$sets .= "bicycle";}

        $User = new User();
        $User->name           = $request->name;
        $User->email          = $request->email;
        $User->phone          = $request->phone;
        $User->password       = bcrypt($request->password);
        $User->image          = HelperController::upload_file('image', '/uploads/users/');
        $User->birth_date     = $request->birth_date;
        $User->about          = $request->about;
        $User->is_active      = $request->is_active;
        $User->gender         = $request->gender;
        $User->vehicles       = $sets;
        $User->nationality    = $request->nationality;
        $User->balance        = $request->balance;
        $User->save();

        $request->session()->flash('status', 'User Created successfully!');
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $user = User::find($id);
        return view('dashboard.users.show', compact('user'));
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        global $request;
        $user = User::destroy($id);
        $request->session()->flash('status', 'User Deleted successfully!');
        return redirect('/users');
    }


    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        $user = User::find($id);
        return view('dashboard.users.update', compact('user'));
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {

        $request->validate([
            //text input 
            'name' => "required|min:3|max:100",
            // email input
            'email' => "required|unique:users,email,$id",
            // phone input
            "phone" => "required|regex:/^\+?\d[0-9-]{7,20}$/|unique:users,phone,$id",
            // password input 
            "password" => 'nullable|string|min:6|confirmed',
            // image input (file)
            'image' => 'nullable|image',
            // date input
            "birth_date" => 'required|date|date_format:Y-m-d',
            // textarea input
            'about' => "required|min:3",
            // radio input 
            "is_active" => 'required|boolean',
            // radio input 
            "gender" => 'required|in:male,female',
            // checkbox input 
            "car" => 'nullable',
            "motorcycle" => 'nullable',
            "bicycle" => 'nullable',
            // select list input 
            "nationality" => 'required|in:Egyptian,Hindi,American',
            // numeric input
            "balance" => 'required|numeric',
        ]);

        $sets = '';
        if(isset($request->car)){$sets .= "car,";}
        if(isset($request->motorcycle)){$sets .= "motorcycle,";}
        if(isset($request->bicycle)){$sets .= "bicycle";}

        $User = User::find($id);
        //text input 
        !isset($request->name)               ? :$User->name                 = $request->name;
        // email input
        !isset($request->email)              ? :$User->email                = $request->email;
        // phone input
        !isset($request->phone)              ? :$User->phone                = $request->phone;
        // password input 
        !isset($request->password)           ? :$User->password             = bcrypt($request->password);
        // image input (file)
        !isset($request->image)              ? :$User->image                = HelperController::upload_file('image', '/uploads/users/');
        // date input
        !isset($request->birth_date)         ? :$User->birth_date           = $request->birth_date;
        // textarea input
        !isset($request->about)              ? :$User->about                = $request->about;
        // radio input 
        !isset($request->is_active)          ? :$User->is_active            = $request->is_active;
        // radio input 
        !isset($request->gender)             ? :$User->gender               = $request->gender;
        // checkbox input 
        !isset($sets)                        ? :$User->vehicles             = $sets;
        // select list input 
        !isset($request->nationality)        ? :$User->nationality          = $request->nationality;
        // numeric input
        !isset($request->balance)            ? :$User->balance              = $request->balance;
        $User->save();

        $request->session()->flash('status', 'User Updated successfully!');
    }
}
