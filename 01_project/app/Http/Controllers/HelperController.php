<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;


class HelperController extends Controller
{
    public static function upload_file($file_name, $directory)
    {
        global $request;
        if ($request->file($file_name) == true) {
            $photo_url = $request->$file_name;
            $photo_url_photo = Str::random(40) . '.' . $photo_url->getClientOriginalExtension();
            $photo_url->move(public_path($directory), $photo_url_photo);
            $full_path_photo_url =  $directory . $photo_url_photo;
            
            return $full_path_photo_url;
        }

        return null;
    }
}
