# Migration

## Generating Migrations
```
php artisan make:migration create_users_table
```

## Generating Schema
```
Schema::create('users', function (Blueprint $table) {
    $table->bigIncrements('id');
    $table->string('user_name', 50);
    $table->text('description');	
    $table->boolean('product_available');
    $table->enum('level', ['easy', 'hard']);	

    $table->decimal('suggested_price', 10, 2);
    $table->tinyInteger('votes');	
    $table->smallInteger('votes');	
    $table->integer('votes');	
    $table->bigInteger('supplier_id');
    
    $table->date('created_at');	
    $table->dateTime('created_at');	
    $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
    $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
    $table->softDeletes();	
});
```

## grant all privileges for the root user.
```
mysql -u root -p
GRANT ALL PRIVILEGES ON database_name.* TO 'root'@'localhost';
FLUSH PRIVILEGES;
```

## Running Migrations
```
php artisan migrate
```