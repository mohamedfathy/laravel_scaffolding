@extends ('dashboard.layouts.02_master')
@section('title', 'الكتب')
@section ('content')
<div class="box">
  <div class="box-header">
    <h3 class="box-title"> الكتب </h3>
  </div>
  <div class="box-header with-border">
    <a href="/bookNew" class="btn btn-primary btn-xs"> اضافة كتاب </a>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <table id="example1" class="table table-bordered table-striped">
      <thead>
      <tr>
          <td> الاسم </td>
          <td>اخر تعديل</td>
          <td>المؤلف</td>
          <td>السعر</td>
          <td>عرض</td>
          <td>تعديل</td>
          <td>حذف</td>
      </tr>
      </thead>
      <tbody>
        @foreach ($Books as $B)
        <tr>
          <td>{{ $B->title }}</td>
          <td>{{ $B->timestamp }}</td>
          <td>{{ $B->author }}</td>
          <td>{{ $B->price }}</td>
          <td><a href="bookShow/{{$B->id}}" class="btn btn-primary btn-xs"> عرض </a></td>
          <td><a href="bookEdit/{{$B->id}}" class="btn btn-success btn-xs"> تعديل </a></td>
          <td><a href="bookDestroy/{{$B->id}}" class="btn btn-danger btn-xs"> حذف </a></td>
        </tr>
        @endforeach
      </tbody>

    </table>
  </div>
  <!-- /.box-body -->
</div>
@endsection