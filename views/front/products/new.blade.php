@extends ('layouts.master')
@section('title', 'اضافة كتاب')
@section ('content')

<div class="box box-primary">

<div class="box-header with-border">
  <h3 class="box-title">اضافة كتاب</h3>
</div><!-- /.box-header -->

<form role="form" method="POST" action="/bookCreate" enctype="multipart/form-data">
	<!-- // Handel the Cross Site Request Forgery -->
	@csrf
	<div class="box-body">

	<div class="form-group">
		<label for="title">العنوان</label>
		<input type="text" name="title" class="form-control" value="{{ old('title') }}">

		@if ($errors->has('title'))
        <span style="color:red;">{{ $errors->first('title') }}</span>
		@endif
	</div><!-- /.form-group -->


	<div class="form-group">
		<label for="about">عن الكتاب</label>
		<input type="text" name="about" class="form-control" value="{{ old('about') }}">

		@if ($errors->has('about'))
        <span style="color:red;">{{ $errors->first('about') }}</span>
		@endif
	</div><!-- /.form-group -->


	<div class="form-group">
		<label for="descripton">الوصف</label>
		<input type="text" name="descripton" class="form-control" value="{{ old('descripton') }}">

		@if ($errors->has('descripton'))
        <span style="color:red;">{{ $errors->first('descripton') }}</span>
		@endif
	</div><!-- /.form-group -->

	<div class="form-group">
		<label for="content">المحتوى</label>
		<input type="text" name="content" class="form-control" value="{{ old('content') }}">

		@if ($errors->has('content'))
        <span style="color:red;">{{ $errors->first('content') }}</span>
		@endif
	</div><!-- /.form-group -->

	<div class="form-group">
		<label for="author">المؤلف</label>
		<input type="text" name="author" class="form-control" value="{{ old('author') }}">

		@if ($errors->has('author'))
        <span style="color:red;">{{ $errors->first('author') }}</span>
		@endif
	</div><!-- /.form-group -->

	<div class="form-group">
		<label for="publication_year">سنة الاصدار</label>
		<input type="number" name="publication_year" class="form-control" value="{{ old('publication_year') }}">

		@if ($errors->has('publication_year'))
        <span style="color:red;">{{ $errors->first('publication_year') }}</span>
		@endif
	</div><!-- /.form-group -->

	<div class="form-group">
		<label for="price">السعر</label>
		<input type="text" name="price" class="form-control" value="{{ old('price') }}">

		@if ($errors->has('price'))
        <span style="color:red;">{{ $errors->first('price') }}</span>
		@endif
	</div><!-- /.form-group -->

	<div class="form-group">
		<label for="url">رابط الكتاب</label>
		<input type="text" name="url" class="form-control" value="{{ old('url') }}">

		@if ($errors->has('url'))
        <span style="color:red;">{{ $errors->first('url') }}</span>
		@endif
	</div><!-- /.form-group -->

	<div class="form-group">
		<label for="cover_url">صورة الغلاف</label>
		<input type="file" name="cover_url" class="form-control" value="{{ old('cover_url') }}">

		@if ($errors->has('cover_url'))
        <span style="color:red;">{{ $errors->first('cover_url') }}</span>
		@endif
	</div><!-- /.form-group -->

	<div class="form-group">
		<label for="is_deleted">محذوف</label>
		<input type="text" name="is_deleted" class="form-control" value="{{ old('is_deleted') }}">

		@if ($errors->has('is_deleted'))
        <span style="color:red;">{{ $errors->first('is_deleted') }}</span>
		@endif
	</div><!-- /.form-group -->

	<div class="form-group">
		<label for="storeId">رقم المتجر</label>
		<input type="text" name="storeId" class="form-control" value="{{ old('storeId') }}">

		@if ($errors->has('storeId'))
        <span style="color:red;">{{ $errors->first('storeId') }}</span>
		@endif
	</div><!-- /.form-group -->

	<div class="form-group">
		<label for="is_trail">تجريب</label>
		<input type="text" name="is_trail" class="form-control" value="{{ old('is_trail') }}">

		@if ($errors->has('is_trail'))
        <span style="color:red;">{{ $errors->first('is_trail') }}</span>
		@endif
	</div><!-- /.form-group -->

	<div class="form-group">
		<label for="is_subscription">التسجيل</label>
		<input type="text" name="is_subscription" class="form-control" value="{{ old('is_subscription') }}">

		@if ($errors->has('is_subscription'))
        <span style="color:red;">{{ $errors->first('is_subscription') }}</span>
		@endif
	</div><!-- /.form-group -->

	<div class="form-group">
		<label for="user_id">المستخدم</label>
		<input type="text" name="user_id" class="form-control" value="{{ old('user_id') }}">

		@if ($errors->has('user_id'))
        <span style="color:red;">{{ $errors->first('user_id') }}</span>
		@endif
	</div><!-- /.form-group -->

	<div class="form-group">
		<label for="category_id">التصنيف</label>
		<input type="text" name="category_id" class="form-control" value="{{ old('category_id') }}">

		@if ($errors->has('category_id'))
        <span style="color:red;">{{ $errors->first('category_id') }}</span>
		@endif
	</div><!-- /.form-group -->


	<div class="box-footer">
		<input type="submit" value="حفظ" name="submit" class="btn btn-primary">
	</div>
</form>
</div>
@endsection