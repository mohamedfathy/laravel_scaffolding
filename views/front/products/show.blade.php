@extends ('layouts.master')
@section('title', 'الكتب')
@section ('content')
<div class="box">
  <div class="box-header">
    <h3 class="box-title"> عرض الكتاب </h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
  <form role="form" method="POST" action="/bookCreate">
  <div class="box-body">

  <div class="form-group">
    <label for="title">العنوان</label>
    <input type="text" name="title" class="form-control" value="{{$Books->title}}">
  </div><!-- /.form-group -->


  <div class="form-group">
    <label for="timestamp">تاريخ التعديل </label>
    <input type="text" name="timestamp" class="form-control" value="{{$Books->timestamp}}">
  </div><!-- /.form-group -->


  <div class="form-group">
    <label for="about">عن الكتاب</label>
    <input type="text" name="about" class="form-control" value="{{$Books->about}}">
  </div><!-- /.form-group -->


  <div class="form-group">
    <label for="descripton">الوصف</label>
    <input type="text" name="descripton" class="form-control" value="{{$Books->descripton}}">
  </div><!-- /.form-group -->

  <div class="form-group">
    <label for="content">المحتوى</label>
    <input type="text" name="content" class="form-control" value="{{$Books->content}}">
  </div><!-- /.form-group -->

  <div class="form-group">
    <label for="author">المؤلف</label>
    <input type="text" name="author" class="form-control" value="{{$Books->author}}">
  </div><!-- /.form-group -->

  <div class="form-group">
    <label for="publication_year">سنة الاصدار</label>
    <input type="text" name="publication_year" class="form-control" value="{{$Books->publication_year}}">
  </div><!-- /.form-group -->

  <div class="form-group">
    <label for="price">السعر</label>
    <input type="text" name="price" class="form-control" value="{{$Books->price}}">
  </div><!-- /.form-group -->

  <div class="form-group">
    <label for="url">رابط الكتاب</label>
    <input type="text" name="url" class="form-control" value="{{$Books->url}}">
  </div><!-- /.form-group -->

  <div class="form-group">
    <label for="cover_url">صورة الغلاف</label>
    <input type="text" name="cover_url" class="form-control" value="{{$Books->cover_url}}">
  </div><!-- /.form-group -->

  <div class="form-group">
    <label for="is_deleted">محذوف</label>
    <input type="text" name="is_deleted" class="form-control" value="{{$Books->is_deleted}}">
  </div><!-- /.form-group -->

  <div class="form-group">
    <label for="storeId">رقم المتجر</label>
    <input type="text" name="storeId" class="form-control" value="{{$Books->storeId}}">
  </div><!-- /.form-group -->

  <div class="form-group">
    <label for="is_trail">تجريب</label>
    <input type="text" name="is_trail" class="form-control" value="{{$Books->is_trail}}">
  </div><!-- /.form-group -->

  <div class="form-group">
    <label for="is_subscription">التسجيل</label>
    <input type="text" name="is_subscription" class="form-control" value="{{$Books->is_subscription}}">
  </div><!-- /.form-group -->

  <div class="form-group">
    <label for="user_id">المستخدم</label>
    <input type="text" name="user_id" class="form-control" value="{{$Books->user_id}}">
  </div><!-- /.form-group -->

  <div class="form-group">
    <label for="category_id">التصنيف</label>
    <input type="text" name="category_id" class="form-control" value="{{$Books->category_id}}">
  </div><!-- /.form-group -->
</form>
  </div>
  <!-- /.box-body -->
</div>
@endsection