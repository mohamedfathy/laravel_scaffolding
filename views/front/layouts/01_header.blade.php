<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="">
    <title>Kedas - @yield('title')</title>
    <link rel="stylesheet" href="/assets/css/vendor/bootstrap_v4.4.1.css">
    <link rel="stylesheet" href="/assets/css/main.css">
    <link rel="stylesheet" href="/assets/css/index.css">
    <link rel="stylesheet" href="/assets/css/contact.css">

    <!-- english font-->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,300,400,700&display=swap" rel="stylesheet">
    <!-- arabic font-->
    <link href="https://fonts.googleapis.com/css?family=Tajawal:400,500,700&display=swap" rel="stylesheet">

    <!-- fontawesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css"
    integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
  </head>
  <body>
    <!--start header-->
    <header class="header">
      <nav class="navbar navbar-expand-lg navbar-dark">
        <div class="container">
          <a class="navbar-brand" href="/{{app()->getLocale() }}"><img src="/assets/images/kedas-logo.png"></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
        
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <div class="flex-grow-1"></div>
            <ul class="navbar-nav text-center">
              <li class="nav-item {{ Request::path() === app()->getLocale() ? 'active ' : '' }}">
                <a class="nav-link" href="{{ route('home', app()->getLocale() )}}">Home</a>
              </li>
              <li class="nav-item {{ Request::path() === app()->getLocale().'/categories' ? 'active ' : '' }}">
                <a class="nav-link" href="{{ route('categories', app()->getLocale() )}}">Products</a>
              </li>
              <li class="nav-item {{ Request::path() === app()->getLocale().'/news' ? 'active ' : '' }}">
                <a class="nav-link" href="{{ route('news', app()->getLocale() )}}">News</a>
              </li>
              <li class="nav-item {{ Request::path() === app()->getLocale().'/about' ? 'active ' : '' }}">
                <a class="nav-link" href="{{ route('about', app()->getLocale() )}}">About</a>
              </li>
              <li class="nav-item {{ Request::path() === app()->getLocale().'/contact' ? 'active ' : '' }}">
                <a class="nav-link" href="{{ route('contact', app()->getLocale() )}}">Contacts</a>
              </li>
            </ul>
          </div>       
        </div>
      </nav>
    </header>
    <!--end header-->