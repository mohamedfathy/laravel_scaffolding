    <!-- start section -->
    <footer class="footer bg-cover" style=" background-image: url('/assets/images/footer-background.jpg');">
      <div class="footer-overlay">
        <div class="container footer-content">
          <div class="row justify-content-between">
            <div class="col-lg-9 order-lg-1 order-2">
              <div class="row justify-content-between">
                <div class="col-md-6">
                  <div class="links">
                    <h5>Contact us</h5>
                    <ul class="list-unstyled footer-links">
                      <li><a href="#"><i class="fas fa-phone mx-1"></i> +20 (0) 86 771 8066 - +20 10 08002015</a></li>
                      <li><a href="#"><i class="fas fa-envelope mx-1"></i> egypt@kedasgroup.de - egypt.kedasgroup.de</a></li>
                      <li><a href="#"><i class="fas fa-map-marker-alt mx-1"></i> 78, Adnan Almalki st - El-Minya - Egypt</a></li>
                      <li><a href="#"><i class="fas fa-map-marker-alt mx-1"></i> 78, Elahad Elgeded st - Samalot - El-Minya - Egypt</a></li>
                    </ul>
                  </div>
                </div>
                
                <div class="col-md-5">
                  <div class="links">
                    <h5>Browse</h5>
                    <ul class="list-unstyled">
                      <li><a href="{{ route('home', app()->getLocale() )}}">Home</a></li>
                      <li><a href="{{ route('categories', app()->getLocale() )}}">Products</a></li>
                      <li><a href="{{ route('news', app()->getLocale() )}}">News</a></li>
                      <li><a href="{{ route('about', app()->getLocale() )}}">About</a></li>
                      <li><a href="{{ route('contact', app()->getLocale() )}}">Contact</a></li>
                    </ul>
                  </div>
                </div>

              </div>
            </div>
            <div class="col-lg-3 order-lg-2 order-1">
              <p class="text-dark footer-heading-text">
                <a href="/{{app()->getLocale()}}">Kedas</a>
              </p>
              <ul class="list-unstyled d-flex align-items-center social-icons">
                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                <li><a href="#"><i class="fab fa-behance"></i></a></li>
              </ul>
            </div>
          </div>
        </div>

        <div style="background-color: rgba(0, 0, 0, 0.1); padding: 20px 0;">
          <p class="text-center text-dark mb-0" >Copyright © 2019 Kedas Company. All rights reserved.</p>
        </div>
      </div>
    </footer>
    <!-- end section -->


    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="/assets/js/vendor/jquery_v3.4.1.js"></script>
    <script src="/assets/js/vendor/popper_v1.6.0.js"></script>
    <script src="/assets/js/vendor/bootstrap_v4.4.1.js"></script>
    <script src="/assets/js/main.js"></script>

    <!-- plugins -->
    <script src="/assets/plugins/photoswipe/photoswipe.min.js"></script> 
    <script src="/assets/plugins/photoswipe/photoswipe-ui-default.min.js"></script> 
    <script src="/assets/plugins/photoswipe/main.js"></script>
  </body>
</html>