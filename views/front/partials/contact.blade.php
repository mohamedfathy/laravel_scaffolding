<!-- start section -->
<section class="contact-us bg-cover" style="background-image: url('/assets/images/footer-background.jpg');">
  <div class="contact-overlay">
    <div class="container">
      <div class="heading text-center mb-4 pt-4">
        <h2 class="text-dark">LET'S TALK</h2>
      </div>
      <style>
      .has-error {
        border:1px solid red;
      }
      </style>
      <form action="/{{app()->getLocale()}}/complaints" method="post" class="needs-validation col-lg-8 m-auto text-white" novalidate>
        @csrf
        <!-- // text input -->
        <div class="form-group col-md-12">
          <label for="name" class="sr-only">Name</label>
          <input type="text" class="form-control contact-input @if ($errors->has('name')) has-error @endif"
           id="name" name="name" placeholder="Name" value="{{ old('name') }}"  required>
          <div class="valid-feedback">Looks good!</div>
          <div class="invalid-feedback">Please provide the name</div>
          @if ($errors->has('name')) <span class="text-danger mx-2"> {{ $errors->first('name') }} </span>@endif
        </div>

        <div class="form-group col-md-12">
          <label for="phone" class="sr-only">Phone</label>
          <input type="tel" class="form-control contact-input  @if ($errors->has('phone')) has-error @endif"
            id="phone" name="phone" placeholder="Phone" value="{{ old('phone') }}"  required>
          <div class="valid-feedback">Looks good!</div>
          <div class="invalid-feedback">Please provide the Phone</div>
          @if ($errors->has('phone')) 
          <span class="text-danger mx-2"> {{ $errors->first('phone') }} </span>
          @endif
        </div>

        <!-- // textarea input -->
        <div class="form-group col-md-12 mb-3">
          <label for="message" class="sr-only">Message</label>
          <textarea class="form-control contact-input  @if ($errors->has('message')) has-error @endif" 
            id="message" name="message" rows="3" placeholder="Message" required>{{ old('message') }}</textarea>
          <div class="valid-feedback">Looks good!</div>
          <div class="invalid-feedback">Please provide the message</div>
          @if ($errors->has('message')) 
          <span class="text-danger mx-2"> {{ $errors->first('message') }} </span>
          @endif
        </div>


        <div class="form-group text-center">
          <button type="submit" class="btn btn-warning main-btn black-btn px-5">Submit</button>
        </div>
      </form>

      <script>
        // Example starter JavaScript for disabling form submissions if there are invalid fields
        (function() {
          'use strict';
          window.addEventListener('load', function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
              form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                  event.preventDefault();
                  event.stopPropagation();
                }
                form.classList.add('was-validated');
              }, false);
            });
          }, false);
        })();
      </script>
    </div>
  </div>
</section>
<!-- end section -->