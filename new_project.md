# Initialize Workspace

## Create New Project.
```
laravel new project_name
```

## Database Configuration
```
DB_DATABASE=darabny
DB_USERNAME=root
DB_PASSWORD=password
```

## Create Migrations
- look in migration.md

## Create Seeders
- look in seeder.md

## Add Resource Route
- look in routes.md

## Add a Model
- look in model.md

## Add a Controller
- look in controller.md

## Adding Arabic validation file.
- copy validation.php to /resources/lang/ar/ directory

## Adding the views
- look in views.md

## change the application name 
### in config/app.php
- 'name' => env('APP_NAME', 'Products')
### in .env
- APP_NAME=Products

## Run cache clear
- php artisan cache:clear
