# Models

## Defining Models
```
php artisan make:model Models/Product
```

## Convention
```
protected $table = 'products';
public $timestamps = false;
```